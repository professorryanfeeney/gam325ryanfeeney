using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    private void Start()
    {
        
    }
    public void OnJump()
    {
        SceneManager.LoadScene("GameOver");
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, 5);
    }
    public void OnRestart()
    {
        SceneManager.LoadScene("Level1");
    }
}
