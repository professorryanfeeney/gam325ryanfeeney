using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour
{
    Animator myAnimator;
    public void Move(Vector2 aInput)
    {
        if (aInput.magnitude > 0)
            myAnimator.SetBool("Moving", true);
        else
            myAnimator.SetBool("Moving", false);


        if (aInput.magnitude > 0.5f)
            myAnimator.SetBool("Running", true);
        else
            myAnimator.SetBool("Running", false);
    }

    public void Attack()
    {
        myAnimator.SetTrigger("Attack");
    }
}
