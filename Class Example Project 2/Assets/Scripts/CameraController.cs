using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (References.Player.gameObject)//this is a null check
            if (StaticMethods.WithinDistance(References.Player.gameObject.transform.position, transform.position, 10))
            {
                if (StaticMethods.WithinDistance(
                    References.Player.gameObject.transform.position, 
                    transform.position, 
                    5))
                {
                    transform.position = 
                        Vector3.MoveTowards(
                            transform.position, 
                            References.Player.gameObject.transform.position, 
                            -10f * Time.deltaTime);
                }
            }
            else
            {
                transform.position = 
                    Vector3.MoveTowards(
                    transform.position, 
                    References.Player.position, 
                    10f * Time.deltaTime);
            }
        //move to be close to the player
    }

}
