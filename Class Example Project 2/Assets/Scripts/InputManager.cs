using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
    private GameObject player;
    private InputActionMap inputMap;

    private void Start()
    {
        inputMap = GetComponent<PlayerInput>().currentActionMap;
        var platforms = GameObject.FindGameObjectsWithTag("Platform");
        foreach (var item in platforms)
        {
            if (onJump == null)
                onJump = item.GetComponent<MovingPlatform>().PlatformFlip;
            else
                onJump += item.GetComponent<MovingPlatform>().PlatformFlip;
        }
    }
    private void Update()
    {
        Vector2 moveInput = inputMap["Move"].ReadValue<Vector2>();
        Vector2 lookInput = inputMap["Look"].ReadValue<Vector2>();

        References.Player.playerController.Move(moveInput);
        References.Player.animationManager.Move(moveInput);

        References.Player.playerController.Look(lookInput);
    }
    public void OnJump()
    {
        if (onJump != null)
            onJump.Invoke();
    }
    public delegate void Callback();
    public Callback onJump;
}
