using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public enum DirectionType
    {
        NULL = 0,
        Forward,
        Backwards,
        Right,
        Left,
        Up,
        Down,
    }
    [SerializeField] private DirectionType direction;
    [SerializeField] private float distance;
    [SerializeField] private float secondsPerPath;
    private bool returning = false;
    private Vector3 startPos;
    private Vector3 endPos;
    private void Start()
    {
        startPos = transform.position;
        switch (direction)
        {
            case DirectionType.Forward:
                endPos = startPos + transform.forward * distance;
                break;
            case DirectionType.Backwards:
                endPos = startPos - transform.forward * distance;
                break;
            case DirectionType.Right:
                endPos = startPos + transform.right * distance;
                break;
            case DirectionType.Left:
                endPos = startPos - transform.right * distance;
                break;
            case DirectionType.Up:
                endPos = startPos + transform.up * distance;
                break;
            case DirectionType.Down:
                endPos = startPos - transform.up * distance;
                break;
            default:
                Debug.LogError("You forgot to set the direction of a moving platform");
                return;
        }
        StartCoroutine(MovingCoroutine());
    }
    private IEnumerator MovingCoroutine()
    {
        Vector3 delta;
        Vector3 prev;
        while (true)
        {
            yield return new WaitForEndOfFrame();
            prev = transform.position;
            if (returning)
            {
                transform.position = Vector3.MoveTowards(transform.position, startPos, 1 / secondsPerPath * Time.deltaTime);
                delta = prev - transform.position;
                foreach (var item in thingsOnMe)
                {
                    item.transform.position -= delta;
                }
                if (StaticMethods.WithinDistance(transform.position, startPos, 0.1f))
                {
                    returning = !returning;
                }
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, endPos, 1 / secondsPerPath * Time.deltaTime);
                delta = prev - transform.position;
                foreach (var item in thingsOnMe)
                {
                    item.transform.position -= delta;
                }
                if (StaticMethods.WithinDistance(transform.position, endPos, 0.1f))
                {
                    returning = !returning;
                }
            }
        }
    }
    List<GameObject> thingsOnMe = new List<GameObject>();
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<Rigidbody>())
        {
            thingsOnMe.Add(collision.gameObject);
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.GetComponent<Rigidbody>())
        {
            thingsOnMe.Remove(collision.gameObject);
        }
    }
    public void PlatformFlip()
    {
        returning = !returning;
    }
}
