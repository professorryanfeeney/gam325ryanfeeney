using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float movementSpeed;
    [SerializeField] private float cameraSensitivity;
    public void Move(Vector2 aMove)
    {
        Vector3 forward = References.mainCamera.transform.forward;
        Vector3 right = References.mainCamera.transform.right;

        //forward.y = References.mainCamera.transform.position.y;
        //right.y = References.mainCamera.transform.position.y;
        Vector3 vel = forward * aMove.y + right * aMove.x;


        vel.y = 0;//fix camera looking down

        //if (vel.magnitude > 1)
            vel = vel.normalized;

        vel *= movementSpeed;
        vel.y = References.Player.rigidbody.velocity.y;
        References.Player.rigidbody.velocity = vel;
    }
    public void Look(Vector2 aLook)
    {
        //References.mainCamera.transform.RotateAround(References.Player.position, new Vector3(aLook.y, 0, 0), cameraSensitivity * Time.deltaTime);
        References.mainCamera.transform.RotateAround(References.Player.position, new Vector3(0, aLook.x, 0), cameraSensitivity * 10 * Time.deltaTime);

        Debug.Log(References.mainCamera.transform.eulerAngles);

        //if (References.mainCamera.transform.eulerAngles.x > 350)
        //    References.mainCamera.transform.eulerAngles =
        //            new Vector3(
        //                References.mainCamera.transform.rotation.x-350,
        //                References.mainCamera.transform.rotation.y,
        //                References.mainCamera.transform.rotation.z
        //                );

        Vector3 rot = References.mainCamera.transform.eulerAngles;
        rot.z = 0;
        References.mainCamera.transform.eulerAngles = rot;
        References.mainCamera.transform.LookAt(References.Player.position);

    }
}
