using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUI : MonoBehaviour
{
    private void Start()
    {
        GetComponent<StatSystem>().AddCallback(OnHealthChange);
    }
    private void OnHealthChange(float aValue, float aMaxValue)
    {
        GameObject.FindGameObjectWithTag("Canvas").
            GetComponent<UICanvas>().
            healthbar.transform.localScale 
            = new Vector3(aValue / aMaxValue, 1, 1);
    }
    private void Update()
    {
        GetComponent<StatSystem>().SubtractValue(Time.deltaTime);//horribly inefficent
    }
}
