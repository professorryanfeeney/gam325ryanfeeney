using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class References : MonoBehaviour
{
    #region PlayerStuff
    private PlayerClass _player;
    public static PlayerClass Player
    {
        get
        {
            if (instance._player == null)
            {
                instance._player = new PlayerClass();
            }
            return instance._player;
        }
    }
    public class PlayerClass
    {
        private GameObject _gameObject;
        public GameObject gameObject
        {
            get
            {
                if (!_gameObject)//this is a null check
                {
                    _gameObject = GameObject.FindGameObjectWithTag("Player");
                }
                return _gameObject;
            }
        }
        private PlayerController _playerController;
        public PlayerController playerController
        {
            get
            {
                if (gameObject != null && _playerController == null)
                {
                    _playerController = gameObject.GetComponent<PlayerController>();
                }
                return _playerController;
            }
        }
        private AnimationManager _animationManager;
        public AnimationManager animationManager
        {
            get
            {
                if (gameObject != null && _playerController == null)
                {
                    _animationManager = gameObject.GetComponent<AnimationManager>();
                }
                return _animationManager;
            }
        }
        private Rigidbody _rigidbody;
        public Rigidbody rigidbody
        {
            get
            {
                if (gameObject != null && _rigidbody == null)
                {
                    _rigidbody = gameObject.GetComponent<Rigidbody>();
                }
                return _rigidbody;
            }
        }
        private Vector3 _position;
        public Vector3 position
        {
            get
            {
                if (gameObject)
                {
                    return gameObject.transform.position;
                }
                return Vector3.zero;
            }
        }
    }
    #endregion
    private GameObject _mainCamera;
    public static GameObject mainCamera
    {
        get
        {
            if (!instance._mainCamera)//this is a null check
            {
                instance._mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
            }
            return instance._mainCamera;
        }
    }
    #region SingletonStuff
    public static References instance;
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }
    #endregion
}
