using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatSystem : MonoBehaviour
{
    public enum StatType
    {
        NULL = 0,
        Health = 1,
        HealthRegen = 2,
        Mana = 3,
        ManaRegen = 6,
        Stamina = 4,
        MoveSpeed = 5,
    }
    [SerializeField] private List<Stat> myStats = new List<Stat>();
    [SerializeField] private Stat myStat;
    [System.Serializable]
    public class Stat
    {
        public StatType statType;
        public float statValue;
        public float maxValue;
        public float minValue;
        public float defaultValue;
    }

    Dictionary<StatType, StatUpdateDelegate> myCallbacks = new Dictionary<StatType, StatUpdateDelegate>();
    public delegate void StatUpdateDelegate(float aValue, float aMaxValue);
    public StatUpdateDelegate OnHealthChange;
    public void AddValue(float aValue)
    {
        //myCallbacks[myStat.statType].
        //myStat.statValue += aValue;
        OnHealthChange?.Invoke(myStat.statValue, myStat.maxValue);
    }
    public void SubtractValue(float aValue)
    {
        myStat.statValue -= aValue;
        OnHealthChange?.Invoke(myStat.statValue, myStat.maxValue);
    }
    public void SetValue(float aValue)
    {
        myStat.statValue = aValue;
        OnHealthChange?.Invoke(myStat.statValue, myStat.maxValue);
    }
    public void AddCallback(StatUpdateDelegate aMethod)
    {
        if (OnHealthChange == null)
        {
            OnHealthChange = aMethod;
        }
        else
        {
            OnHealthChange += aMethod;
        }
    }
    //private void Start()
    //{
    //    if (OnStatChange == null)
    //    {
    //        OnStatChange = UpdateUI;
    //    }
    //    else
    //    {
    //        OnStatChange += UpdateUI;
    //    }
    //}
    //private void UpdateUI()
    //{
    //    //do a thing
    //}
    //private void Update()
    //{
    //    OnStatChange?.Invoke();
    //}

    //int health2 = 1;
    //int mana = 2;
    //public void ExampleMethod()
    //{
    //    string health = "Health";
    //    string healthy = "Healthy";
    //    if(health == healthy)//bad on performance
    //    {
    //        //do a thing
    //    }
    //    if(health2 == mana)//hard to read
    //    {
    //        //do a thing
    //    }
    //    if (myStat == StatType.Health)
    //    {
    //        //do a thing
    //        Debug.Log(myStat.ToString());
    //    }
    //}
}
