using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StaticMethods
{
    public static bool WithinDistance(Vector3 aStart, Vector3 aEnd, float aDistance)
    {
        float xDiff = aStart.x - aEnd.x;
        float yDiff = aStart.y - aEnd.y;
        float zDiff = aStart.z - aEnd.z;
        float sqrDistance = xDiff * xDiff + yDiff * yDiff + zDiff * zDiff;

        if (sqrDistance > aDistance * aDistance)
            return false;
        else
            return true;
    }
    public static float Distance(Vector3 a, Vector3 b)
    {
        float num1 = a.x - b.x;
        float num2 = a.y - b.y;
        float num3 = a.z - b.z;
        return Mathf.Sqrt(num1 * num1 + num2 * num2 + num3 * num3);
    }
}
