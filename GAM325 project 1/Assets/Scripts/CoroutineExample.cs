using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineExample : MonoBehaviour
{
    private IEnumerator movingCoroutine;
    //private IEnumerator OthermovingCoroutine;
    private void Start()
    {
        movingCoroutine = MovingCoroutine();
        //OthermovingCoroutine = MovingCoroutine();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            StartCoroutine(movingCoroutine);
            //StartCoroutine(OthermovingCoroutine);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //StopAllCoroutines();
            StopCoroutine(movingCoroutine);
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            if (!reloading)
            {
                Debug.Log("Bang");
                StartCoroutine(CooldownCoroutine(5));
            }
            else
            {
                Debug.Log("Reloading!");
            }
        }
    }

    private IEnumerator MovingCoroutine()
    {
        while (true)
        {
            transform.position += transform.right * Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }
    bool reloading = false;
    private IEnumerator CooldownCoroutine(int aSeconds)
    {
        reloading = true;
        //do logic
        yield return new WaitForSeconds(aSeconds);
        reloading = false;
    }
}
