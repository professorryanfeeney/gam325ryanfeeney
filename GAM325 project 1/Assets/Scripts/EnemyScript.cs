using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private Rigidbody playerRb;
    [SerializeField] private PlayerScore playerScore;
    private Rigidbody myRigidbody;
    [SerializeField] float movementSpeed;
    // Start is called before the first frame update
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody>();
        if (player != null)
        {
            playerRb = player.GetComponent<Rigidbody>();
            playerScore = player.GetComponent<PlayerScore>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 lookAtMe = player.transform.position;
        lookAtMe.y = transform.position.y;
        transform.LookAt(lookAtMe);
        myRigidbody.velocity = transform.forward * movementSpeed;


        if (ShouldIExplode())
            Explode();
    }
    private bool ShouldIExplode()
    {
        Vector3 a = player.transform.position;
        Vector3 b = transform.position;
        float num = a.x - b.x;
        float num2 = a.y - b.y;
        float num3 = a.z - b.z;
        float c = (num * num + num2 * num2 + num3 * num3);

        if (c <= 2 * 2)
            return true;

        return false;
    }
    private void Explode()
    {
        if (playerRb != null)
            playerRb.AddForce(transform.forward * 500 + transform.up * 50);

        if (playerScore != null)
            playerScore.score -= 100;
        var temp = GameObject.FindGameObjectsWithTag("Player");

        GameObject myObj = GameObject.FindGameObjectWithTag("Finish");
        //try
        //{
        //    myObj.GetComponent<PlayerScore>().score -= 100;
        //}
        //catch
        //{
        //    Debug.LogError("Terribile exeption nooo");
        //}
        if (myObj != null)
            myObj.GetComponent<PlayerScore>().score -= 100;

        Singleton.instance.Score -= 100;
    }
    
}
