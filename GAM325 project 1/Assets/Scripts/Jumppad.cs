using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumppad : MonoBehaviour
{
    [SerializeField] float waterBoyancy;
    [SerializeField] float waterCap;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerStay(Collider other)
    {
        if (other == null)
            return;

        if (other.tag != "Player")
        {
            return;
        }

        Rigidbody myRigidbody = other.GetComponent<Rigidbody>();

        if (myRigidbody != null)
        {
            if (myRigidbody.velocity.y < waterCap)
                myRigidbody.AddForce(new Vector3(0, waterBoyancy, 0));
        }
    }
}
