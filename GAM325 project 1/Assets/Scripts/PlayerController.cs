using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [HideInInspector] public Rigidbody myRigidbody;
    [SerializeField] private float movementSpeed;
    [SerializeField] private GameObject projectilePrefab;
    //public float movementSpeed;
    // Start is called before the first frame update
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        //myRigidbody.velocity = new Vector3(horizontal * movementSpeed, myRigidbody.velocity.y, vertical * movementSpeed);
        Vector3 vel = transform.forward * vertical + transform.right * horizontal;

        if (vel.magnitude > 1)
            vel = vel.normalized;

        vel *= movementSpeed;
        vel.y = myRigidbody.velocity.y;
        myRigidbody.velocity = vel;


        float mouseX = Input.GetAxis("Mouse X") * 2;
        float mouseY = Input.GetAxis("Mouse Y") * 2;

        transform.eulerAngles += new Vector3(-mouseY, mouseX, 0);

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            GameObject temp = Instantiate(projectilePrefab, transform.position + transform.forward, transform.rotation);

            temp.GetComponent<Rigidbody>().velocity = temp.transform.forward * 10 + temp.transform.up*2;

            Destroy(temp, 5);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            myRigidbody.AddForce(new Vector3(0, 500, 0));
        }

    }
}




































//if (Input.GetKeyDown(KeyCode.Mouse0))
//{
//    GameObject temp = Instantiate(projectilePrefab, transform.position + transform.forward, transform.rotation);
//    temp.GetComponent<Rigidbody>().AddForce(temp.transform.forward * 500 + temp.transform.up * 100);
//    Destroy(temp, 2);
//}
