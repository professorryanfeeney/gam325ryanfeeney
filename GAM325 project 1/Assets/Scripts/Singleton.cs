using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton : MonoBehaviour
{
    public static Singleton instance;
    private GameObject player;
    public GameObject Player
    {
        get
        {
            if (player == null)
            {
                player = GameObject.FindGameObjectWithTag("Player");
            }
            if (player == null)
            {
                Debug.LogError("Game Manager asked for player and none was found");
            }
            return player;
        }
    }
    private Rigidbody playerRigidbody;
    public Rigidbody PlayerRigidbody
    {
        get
        {
            if (playerRigidbody == null)
            {
                playerRigidbody = player.GetComponent<Rigidbody>();
            }
            if (playerRigidbody == null)
            {
                Debug.LogError("Game Manager asked for playerRigidbody and none was found");
            }

            return playerRigidbody;
        }
    }

    private int score = 0;
    public int Score
    {
        get
        {
            return score;
        }
        set
        {
            //Debug.Log("Setting Score to " + value);
            if (value < 0)
            {
                score = 0;
                return;
            }
            score = value;
        }
    }


    public void SetScore(int aParam)
    {
        score = aParam;
    }
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }
}
