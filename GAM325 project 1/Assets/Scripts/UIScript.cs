using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIScript : MonoBehaviour
{
    [SerializeField] private Text myText;
    private float timer = 0;
    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        myText.text = ((int)timer).ToString();
        if(timer>5)
        {
            SceneManager.LoadScene("EndScreen");
        }
    }
}
