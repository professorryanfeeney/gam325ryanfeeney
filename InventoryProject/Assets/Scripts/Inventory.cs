using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    private List<ItemInstance> items = new List<ItemInstance>();
    private ItemInstance equipped;


    //This would be another way to handle the inventory
    //Just set equippedIndex to -1 in drop logic when you remove it from the list
    /*
    private int equippedIndex = -1;
    private ItemInstance equipped
    {
        get
        {
            if (equippedIndex == -1)
                return null;

            return items[equippedIndex];
        }
    }
    */

    public void PickUpItem(ItemInstance aItem)
    {
        items.Add(aItem);
        if (equipped == null)
        {
            Equip(aItem);
            Equip(0);
        }
    }
    public void Equip(int aIndex)
    {
        equipped = items[aIndex];
    }
    public void Equip(ItemInstance aItem)
    {
        equipped = aItem;
    }
    public void Drop()
    {
        items.Remove(equipped);
        equipped = null;
    }
    public ItemInstance GetEquippedItem()
    {
        return equipped;
    }
}
