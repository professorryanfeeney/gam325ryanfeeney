using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemCollision : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        Inventory inv = other.GetComponent<Inventory>();
        if (!inv)
            return;

        inv.PickUpItem(GetComponent<ItemInstance>());

        Destroy(this.gameObject);
    }
}
