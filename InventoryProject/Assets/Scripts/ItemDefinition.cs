using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//https://docs.unity3d.com/Manual/class-ScriptableObject.html
[CreateAssetMenu(fileName = "Data", menuName = "ItemDefinition", order = 1)]
public class ItemDefinition : ScriptableObject
{
    public int damage;
}
