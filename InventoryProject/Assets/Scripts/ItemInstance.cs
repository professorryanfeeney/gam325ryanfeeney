using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemInstance : MonoBehaviour
{
    [SerializeField] private ItemDefinition myDefinition;

    public ItemDefinition GetDefintion()
    {
        return myDefinition;
    }
}
