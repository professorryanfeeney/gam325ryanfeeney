using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Inventory _myInventory;
    private Inventory myInventory
    {
        get
        {
            if (!_myInventory)
            {
                _myInventory = GetComponent<Inventory>();
            }
            return _myInventory;
        }
    }
    private void Update()
    {
        PlayerInputs();
    }
    private void PlayerInputs()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Attack();
        }
    }
    private void Attack()
    {
        if (myInventory)
        {
            Debug.Log("I deal " + myInventory.GetEquippedItem().GetDefintion().damage + " damage!");
        }
        else
        {
            //throw a punch do 1 damage
        }
    }
}
