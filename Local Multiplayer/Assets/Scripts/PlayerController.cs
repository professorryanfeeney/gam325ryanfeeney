using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Rigidbody2D myRigidbody;
    void Update()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
        References.AddPlayer(this.gameObject);
    }
    public void OnJump()
    {
        myRigidbody.velocity = new Vector2(0, 5);
    }
}
