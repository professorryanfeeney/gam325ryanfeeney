using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class References : MonoBehaviour
{
    [SerializeField] private GameObject PlayerPrefab;
    PlayerInputManager myPlayerInputManager;
    public static References instance;
    private void Awake()
    {
        if(instance!=null)
        {
            Destroy(this.gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(this.gameObject);
        SetReferences();
    }
    private void SetReferences()
    {
        myPlayerInputManager = GetComponent<PlayerInputManager>();
        myPlayerInputManager.playerPrefab = PlayerPrefab;
    }
    public List<Player> players = new List<Player>();
    public class Player
    {
        public GameObject gameObject;
        public Player(GameObject aGameObject)
        {
            gameObject = aGameObject;
        }
    }
    public static void AddPlayer(GameObject aGameObject)
    {
        instance.players.Add(new Player(aGameObject));
    }
    //public static void AddPlayer()
    //{
    //    instance.players.Add(new Player());
    //}
}
