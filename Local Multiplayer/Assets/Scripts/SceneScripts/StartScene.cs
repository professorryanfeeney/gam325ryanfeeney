using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartScene : MonoBehaviour
{
    [SerializeField] private Text playerCountTxt;
    private int playerCount = 0;
    InputDevice device;
    public void OnJump()
    {
        References.instance.GetComponent<PlayerInputManager>().JoinPlayer(playerCount++ + 1, -1, null, device);
        playerCountTxt.text = playerCount.ToString();
    }
    public void OnStart()
    {

    }
}
