using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MyAIScript : MonoBehaviour
{
    [SerializeField] GameObject goal;
    private NavMeshAgent myNavMeshAgent;
    private void Start()
    {
        myNavMeshAgent = GetComponent<NavMeshAgent>();
    }
    private void Update()
    {
        myNavMeshAgent.destination = goal.transform.position;
    }
}
