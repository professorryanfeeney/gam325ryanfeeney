using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuScript : MonoBehaviour
{
    [SerializeField] List<GameObject> tabs;
    private int tabIndex = 1;
    private void Start()
    {
        foreach (var item in tabs)
        {
            item.SetActive(false);
        }

        if (tabs.Count > 0)
            tabs[0].SetActive(true);
    }
    private void Update()
    {
        PlayerInput();
    }
    private void PlayerInput()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            TabLeft();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            TabRight();
        }
    }
    public void TabLeft()
    {
        if (tabIndex == 1)
        {
            tabs[tabIndex - 1].SetActive(false);
            tabIndex = tabs.Count;
            tabs[tabIndex - 1].SetActive(true);
        }
        else
        {
            tabs[tabIndex - 1].SetActive(false);
            tabIndex--;
            tabs[tabIndex - 1].SetActive(true);
        }
    }
    public void TabRight()
    {
        if(tabIndex>tabs.Count)
        {
            tabs[tabIndex - 1].SetActive(false);
            tabIndex = 1;
            tabs[tabIndex - 1].SetActive(true);
        }
        else
        {
            tabs[tabIndex - 1].SetActive(false);
            tabIndex++;
            tabs[tabIndex - 1].SetActive(true);
        }
    }
    public void PrintTabNumber(int aTabId)
    {
        Debug.Log("I am on tab " + aTabId);
    }
}
