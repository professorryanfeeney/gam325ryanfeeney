using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] GameObject pauseMenu;
    private void Start()
    {
        pauseMenu.SetActive(false);
    }
    private void Update()
    {
        PlayerInput();
    }
    private void PlayerInput()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            PauseGame();
        }
    }
    private bool paused = false;
    private void PauseGame()
    {
        if (paused)
        {
            Time.timeScale = 1;
            paused = false;
            pauseMenu.SetActive(false);
        }
        else
        {
            Time.timeScale = 0; 
            paused = true;
            pauseMenu.SetActive(true);
        }
    }
}
